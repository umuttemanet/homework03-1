/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Homework3;

/**
 *
 * @author erene
 */
public class Student {
    
 private String name;
 
 private String department;
 private double pay;
 
    
public Student (String name, String Department, Double Pay){
    this.name=name;
    this.department=Department;
    this.pay=Pay;
} 

    public Student(String name){
        this(name,"Computer developer", 10000.0);
    }
public void setDepartment(String Department){
    this.department=Department;
}
public void setPay(double pay){
    this.setGpa(pay);
}
public void  giveRaise(double raisePercent){
    this.pay=this.pay*(1+raisePercent);
}

 @Override
 public String toString(){
    String output=String.format ("[Person:  %s , Department: %s,Pay :%.2f]", name, department, pay);  
    return output;
}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
public String getLastName(){
    String[] tokens = name.split(" ");
    return tokens[1];
}
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @return the pay
     */
    public double getGpa() {
        return pay;
    }

    /**
     * @param pay the pay to set
     */
    public void setGpa(double pay) {
        this.pay = pay;
    }
}
